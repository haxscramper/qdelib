#include "QDELib.hpp"
#include <iostream>

std::vector<std::string> tagsetToTaglist(spt::TagSet* set) {
    std::vector<std::string> res;
    for (const spt::Tag& tag : set->getTagsCRef()) {
        res.push_back(tag.toString().toStdString());
    }
    return res;
}

void QSLink::setURL(std::string url) {
    impl.setURL(QString::fromStdString(url));
}


std::string QSLink::getURL() const {
    return impl.getURL().toString().toStdString();
}


void QSLink::setLinkTags(std::vector<std::string> tags) {
    impl.setLinkTags(spt::TagSet::parseFromTagList(tags));
}


bool QSLink::openFile(std::string absPath, bool appendExtension) {
    return impl.openFile(QString::fromStdString(absPath), appendExtension);
}


bool QSLink::saveFile(
    std::string absPath,
    bool        appendExtension,
    bool        autoCreatePath) {
    return impl.saveFile(
        QString::fromStdString(absPath), appendExtension, autoCreatePath);
}


std::vector<std::string> QSLink::getLinkTags() {
    return tagsetToTaglist(impl.getLinkTagsPtr());
}


void QSLink::setMetaDescription(std::string description) {
    impl.setMetaDescription(QString::fromStdString(description));
}


std::string QSLink::getMetaDescription() const {
    return impl.getMetaDescription().toStdString();
}


void QSLink::setMetaNote(std::string note) {
    impl.setMetaNote(QString::fromStdString(note));
}


std::string QSLink::getMetaNote() const {
    return impl.getMetaNote().toStdString();
}


void QSLink::addLinkTag(std::string tag) {
    impl.addLinkTag(
        spt::Tag::parseFromString(QString::fromStdString(tag)));
}


void QSLink::setContentText(std::string note) {
    impl.setContentText(QString::fromStdString(note));
}


std::string QSLink::getContentText() const {
    return impl.getContentText().toStdString();
}

//#= QSNote

void QSNote::setMetaDescription(std::string description) {
    impl.setMetaDescription(QString::fromStdString(description));
}

void QSNote::setMetaNote(std::string note) {
    impl.setMetaNote(QString::fromStdString(note));
}

void QSNote::setNoteText(std::string text) {
    impl.setNoteText(QString::fromStdString(text));
}


void QSNote::setNoteTags(std::vector<std::string> tags) {
    impl.setNoteTagsByVal(spt::TagSet::parseFromTagList(tags));
}

void QSNote::addNoteTag(std::string tag) {
    impl.addNoteTag(spt::Tag::parseFromString(tag));
}

void QSNote::setMetaTags(std::vector<std::string> tags) {
    impl.setMetaTagsByVal(spt::TagSet::parseFromTagList(tags));
}

void QSNote::addMetaTag(std::string tag) {
    impl.addMetaTag(spt::Tag::parseFromString(tag));
}

bool QSNote::openFile(std::string absPath, bool appendExtension) {
    return impl.openFile(QString::fromStdString(absPath), appendExtension);
}


bool QSNote::saveFile(
    std::string absPath,
    bool        appendExtension,
    bool        autoCreatePath) {
    return impl.saveFile(
        QString::fromStdString(absPath), appendExtension, autoCreatePath);
}

//#= QSTodo

QSTodo::QSTodo() {
    impl = std::make_shared<qde::QSTodo>();
}

void QSTodo::setTaskDescription(std::string description) {
    impl->setTaskDescription(QString::fromStdString(description));
}

void QSTodo::setTodoTags(std::vector<std::string> tags) {
    impl->setTodoTags(spt::TagSet::parseFromTagList(tags));
}

void QSTodo::addTodoTag(std::string tag) {
    impl->addTodoTag(
        spt::Tag::parseFromString(QString::fromStdString(tag)));
}

void QSTodo::setCompletionLikelihood(QSTodo::CompletionLikelihood value) {

    qde::QSTodo::CompletionLikelihood implCompletion;
    using Enum    = CompletionLikelihood;
    using EnumQDE = qde::QSTodo::CompletionLikelihood;

    switch (value) {
        case Enum::None: implCompletion = EnumQDE::None; break;
        case Enum::Small: implCompletion = EnumQDE::Small; break;
        case Enum::Medium: implCompletion = EnumQDE::Medium; break;
        case Enum::High: implCompletion = EnumQDE::High; break;
        case Enum::Guaranteed: implCompletion = EnumQDE::Guaranteed; break;
    }

    impl->setCompletionLikelihood(implCompletion);
}

void QSTodo::setStatus(QSTodo::Status value) {
    qde::QSTodo::Status implStatus;
    using Enum    = Status;
    using EnumQDE = qde::QSTodo::Status;

    switch (value) {
        case Enum::None: implStatus = EnumQDE::None; break;
        case Enum::NeedsAction: implStatus = EnumQDE::NeedsAction; break;
        case Enum::InProgress: implStatus = EnumQDE::InProgress; break;
        case Enum::Completed: implStatus = EnumQDE::Completed; break;
        case Enum::Cancelled: implStatus = EnumQDE::Cancelled; break;
        case Enum::Postponed: implStatus = EnumQDE::Postponed; break;
        case Enum::Deleted: implStatus = EnumQDE::Deleted; break;
        case Enum::Nuked: implStatus = EnumQDE::Nuked; break;
        case Enum::Stalled: implStatus = EnumQDE::Stalled; break;
    }

    impl->setStatus(implStatus);
}

void QSTodo::setPriority(QSTodo::Priority value) {
    qde::QSTodo::Priority implPriority;
    using Enum    = Priority;
    using EnumQDE = qde::QSTodo::Priority;

    switch (value) {
        case Enum::None: implPriority = EnumQDE::None;
        case Enum::NoPriority: implPriority = EnumQDE::NoPriority;
        case Enum::Low: implPriority = EnumQDE::Low;
        case Enum::Medium: implPriority = EnumQDE::Medium;
        case Enum::High: implPriority = EnumQDE::High;
        case Enum::Critical: implPriority = EnumQDE::Critical;
        case Enum::Blocking: implPriority = EnumQDE::Blocking;
        case Enum::CurrentlyWorking:
            implPriority = EnumQDE::CurrentlyWorking;
        case Enum::Organization: implPriority = EnumQDE::Organization;
        case Enum::Later: implPriority = EnumQDE::Later;
    }

    impl->setPriority(implPriority);
}

void QSTodo::setNecessity(QSTodo::Necessity value) {
    qde::QSTodo::Necessity implNecessity;
    using Enum    = Necessity;
    using EnumQDE = qde::QSTodo::Necessity;

    switch (value) {
        case Enum::None: implNecessity = EnumQDE::None;
        case Enum::Useless: implNecessity = EnumQDE::Useless;
        case Enum::Useful: implNecessity = EnumQDE::Useful;
        case Enum::VeryUseful: implNecessity = EnumQDE::VeryUseful;
        case Enum::Important: implNecessity = EnumQDE::Important;
        case Enum::VeryImportant: implNecessity = EnumQDE::VeryImportant;
    }

    impl->setNecessity(implNecessity);
}

void QSTodo::setEstimatedTime(QSTodo::EstimatedTime value) {
    qde::QSTodo::EstimatedTime implEstimate;
    using Enum    = EstimatedTime;
    using EnumQDE = qde::QSTodo::EstimatedTime;

    switch (value) {
        case Enum::None: implEstimate = EnumQDE::None;
        case Enum::_5_10_min: implEstimate = EnumQDE::_5_10_min;
        case Enum::_10_30_min: implEstimate = EnumQDE::_10_30_min;
        case Enum::_1_2_hours: implEstimate = EnumQDE::_1_2_hours;
        case Enum::_3_6_hours: implEstimate = EnumQDE::_3_6_hours;
        case Enum::_1_2_days: implEstimate = EnumQDE::_1_2_days;
        case Enum::_5_6_days: implEstimate = EnumQDE::_5_6_days;
        case Enum::_1_3_weeks: implEstimate = EnumQDE::_1_3_weeks;
        case Enum::_1_2_months: implEstimate = EnumQDE::_1_2_months;
        case Enum::_5_6_months: implEstimate = EnumQDE::_5_6_months;
        case Enum::_forever: implEstimate = EnumQDE::_forever;
    }

    impl->setEstimatedTime(implEstimate);
}

void QSTodo::saveFile(std::string absPath) {
    impl->saveFile(QString::fromStdString(absPath));
}

void QSTodo::appendNestedTodo(QSTodo todo) {
    impl->appendNestedTodo(todo.impl->createUptrCopy());
}

void QSTodo::setCustomData(std::string identifier, std::string content) {
    impl->setCustomData({QString::fromStdString(identifier),
                         QString::fromStdString(content)});
}
