#!/usr/bin/env python

from QDELib import *
import os

note = QSNote()
link = QSLink()
todo = QSTodo()

link = QSLink()
link.setURL("Hello world")
link.setLinkTags(["random", "tag"])
link.setMetaDescription("Random description string")
link.setMetaNote("TODO: save examples into separate files")

link.saveFile(os.path.abspath("test.qslink"))

link_2 = QSLink()
link.openFile(os.path.abspath("test.qslink"))

todo = QSTodo()
nested = QSTodo()

nested.setTaskDescription("Some random description")
todo.appendNestedTodo(nested)

print(" -> Added nested todo")
path = os.path.join(os.getcwd(), "test.qstodo")
todo.saveFile(path)
print(" -> Saved top todo with nested at", path)

exit(0)
