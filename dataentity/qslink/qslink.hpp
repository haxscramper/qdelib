#pragma once

/*!
 * \file qslink.hpp
 */

//===    Qt    ===//
#include <QDebug>
#include <QString>
#include <QStringList>
#include <QTextDocument>
#include <QtCore>
#include <QtXml>


//=== Sibling  ===//
#include "../base/dataitem.hpp"
#include "qslink_xmlreader.hpp"

//  ////////////////////////////////////


namespace qde {
/*!
 * \brief Represents URL with associated metadata
 * (url tags, downloaded html source)
 */
class QSLink : public DataItem
{
    friend void xml::readQSLinkXML(
        QSLink*           target,
        QXmlStreamReader* xmlStream,
        void*             _tags);

  public:
    struct QSLinkXMLTags : BaseMetadata::XMLTags {
        struct {
            QString url         = "url";
            QString linkTags    = "link-tags";
            QString contentText = "content-text";
            QString content     = "qslink-content";
            QString rootName    = "qslink-root";
            QString extension   = "qslink";
        } qslink;
    };

    virtual ~QSLink() override = default;
    void    setURL(QUrl url);
    void    setURL(QString url);
    QUrl    getURL() const;
    void    setContentText(QString text);
    QString getContentText() const;
    void    setLinkTags(spt::TagSet tags);
    /// \todo Rename into getLinkTagsPtr and add function that returns
    /// QStringList
    spt::TagSet  getLinkTags();
    spt::TagSet* getLinkTagsPtr();
    void         addLinkTag(spt::Tag tag);
    bool         operator==(const QSLink& other) const;

  private:
    static QSLinkXMLTags xmlTags;

    QUrl    linkUrl;     ///< Urk
    QString contentText; ///< Downloaded content of the page. Might be html
                         ///< or processed text
    spt::TagSet urlTags; ///< Tags associated with given link

    //#= DataObject interface
  public:
    QDomElement toXML(QDomDocument& document) const override;
    QString     getExtension(bool prependDot = false) const override;
    void        fromXML(QXmlStreamReader* xmlStream) override;
    void readFile(QString path, bool appendExtension = false) override;
    void writeFile(QString path, bool appendExtension = false) override;
    bool saveFile(
        QString path,
        bool    appendExtension = false,
        bool    autoCreate      = true) override;
    bool    openFile(QString path, bool appendExtension = false) override;
    QString toDebugString(
        size_t indentationLevel = 0,
        bool   printEverything  = false,
        int    recurseLevel     = -1) override;
};
} // namespace qde


Q_DECLARE_METATYPE(qde::QSLink*)
