#include "qsevent_xmlreader.hpp"
#include "qsevent.hpp"
#include <QXmlStreamReader>

namespace qde {
namespace xml {


    static void readQSEventContent(
        QSEvent*                 target,
        QXmlStreamReader*        xmlStream,
        QSEvent::QSEventXMLTags* tags) {
        while (xmlStream->readNextStartElement()) {
            if (xmlStream->name() == tags->qsevent.eventTags) {
                target->setEventTags(spt::TagSet::fromXML(
                    xmlStream, tags->qsevent.eventTags));
            } else if (xmlStream->name() == tags->qsevent.repeatTimes) {
                target->setRepeatTimes(
                    xmlStream->readElementText().toInt());
            } else if (xmlStream->name() == tags->qsevent.repeatInterval) {
                target->setRepeatInterval(QDateTime::fromString(
                    xmlStream->readElementText(), Qt::ISODate));
            } else {
                xmlStream->skipCurrentElement();
            }
        }
    }


    void readQSEventXML(
        QSEvent*          target,
        QXmlStreamReader* xmlStream,
        void*             _tags) {

        if (xmlStream->name().isEmpty()) {
            xmlStream->readNextStartElement();
        }

        QSEvent::QSEventXMLTags* tags;
        if (_tags == nullptr) {
            tags = &target->xmlTags;
        } else {
            tags = static_cast<QSEvent::QSEventXMLTags*>(_tags);
        }


        while (xmlStream->readNextStartElement()) {
            if (xmlStream->name() == tags->base.section) {
                target->getBaseMetadataRef().readXML(xmlStream);
            } else if (xmlStream->name() == tags->qsevent.content) {
                readQSEventContent(target, xmlStream, tags);
            } else {
                xmlStream->skipCurrentElement();
            }
        }
    }
} // namespace xml
} // namespace qde
