#pragma once

#    include <boost/multi_array.hpp>
#    include <boost/variant.hpp>
#    include <vector>
#    include <algorithm>
#    include <exception>
#    include <QVariant>
#    include "../base/datacontainer.hpp"

typedef boost::multi_array<dent::ContainerItem, 3> Tensor;
typedef Tensor::array_view<3>::type                TensorView;
typedef boost::multi_array_types::index_range      Range;



// TODO Do not resize code editor after new file opened
