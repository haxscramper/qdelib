#include "containeritem.hpp"

namespace qde {
ContainerItem::ContainerItemXMLTags ContainerItem::xmlTags;

ContainerItem::ContainerItem(const ContainerItem& other) {
    Q_UNUSED(other);
}


ContainerItem& ContainerItem::operator=(ContainerItem&& other) {
    data = std::move(other.data);
    return *this;
}

ContainerItem& ContainerItem::operator=(const SimpleData& other) {
    data = other;
    return *this;
}

bool ContainerItem::operator==(const ContainerItem& other) {
    if (isSimple() && other.isSimple()) {
        return std::get<0>(data) == std::get<0>(other.data);
    } else if (!(isSimple() && other.isSimple())) {
        return (*std::get<1>(data)) == (*std::get<1>(data));
    } else {
        return false;
    }
}


bool ContainerItem::isSimple() const {
    return data.index() == 0;
}


ContainerItem& ContainerItem::operator=(const ContainerItem& other) {
    Q_UNUSED(other) return *this;
}


QDomElement ContainerItem::toXML(QDomDocument& document) {
    QDomElement XML_root = document.createElement(this->getXMLRoot());
    return XML_root;
}

QVariant ContainerItem::toVariant() {
    return QVariant();
}


void ContainerItem::setContent(SimpleData _data) {
    data = _data;
}

} // namespace qde
