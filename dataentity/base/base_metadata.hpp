#pragma once

//===    Qt    ===//
#include <QDateTime>
#include <QDomDocument>
#include <QDomElement>
#include <QDomNode>
#include <QFile>
#include <QStringList>
#include <QTextStream>
#include <QUuid>
#include <QXmlStreamReader>

//=== Internal ===//
#include <hsupport/misc.hpp>
#include <hsupport/tagset.hpp>
#include <hsupport/version.hpp>


namespace qde {

/// Abstract base class for all classse that implement support for
/// serialization/deserialization to/from xml. Derived class might also
/// implement static function `static Class fromXML(QXmlReader* xmlStream)`
/// for creating new object from xml stream
class XMLReader
{
    /// Export content into QDomElement
    virtual QDomElement toXML(QDomDocument& document) const = 0;
    /// Fill fields on the objects by reading xml stream
    virtual void readXML(QXmlStreamReader* xmlStream) = 0;
};

/// \note Because it is not possible to define static virtual function that
/// will have different return types for derived classes it is necessary to
/// either manually write bolierplate code or delegate this job to macro.
///
/// This macro should be used in *public* section of class definition.
/// Class should be derived from XMLReader or have `fromXML` function that
/// accepts pointer to QXmlStreamReader. \arg Class - name of the class to
/// create instance of and read value into.
#define XML_READER_DEFINE_FROM_XML(Class)                                 \
    static Class fromXML(QXmlStreamReader* xmlStream) {                   \
        Class res;                                                        \
        res.readXML(xmlStream);                                           \
        return res;                                                       \
    }

/// Uuid/exact name. Used to index data entities by names/uuids
struct DataDescriptor : public XMLReader {
    /// QUuid
    QUuid uuid;
    /// Optional qstring for exactName
    std::optional<QString> exactName;

    bool operator==(const DataDescriptor& other) const;

    //#= XML manipulation
    QDomElement toXML(QDomDocument& document) const override;
    void        readXML(QXmlStreamReader* xmlStream) override;
    XML_READER_DEFINE_FROM_XML(DataDescriptor);
};

struct Attachment : public XMLReader {
    QString name;
    QString path;
    //#= XML manipulation
    QDomElement toXML(QDomDocument& document) const override;
    void        readXML(QXmlStreamReader* xmlStream) override;
    XML_READER_DEFINE_FROM_XML(Attachment);
};

/// \brief Common attribute of any type of information
struct BaseMetadata : public XMLReader {
    /*!
     * This filed will be used as locally unique non-human-readable
     * identifier that might be used for various purposes (building indexes
     * and performing interlinking for example).
     */
    QString      exactName;
    spt::TagSet  tags;         ///< List of tags applied to this DataEntity
    QString      description;  ///< Description for content of this object
    QString      name;         ///< Textual name of data
    QDateTime    editDateTime; ///< DateTime of modification
    QString      metaNote;     ///< Textual notes about the data
    QString      source;     ///< Textual information about origins of data
    QUuid        uuid;       ///< DataEnity's UUID
    spt::Version version;    ///< DataEntity version
    spt::Version xmlVersion; ///< Version of xml layout
    QDateTime    creationDateTime; ///< DateTime of creation

    /// Field for custom user data (plugins). Section name-content pair
    std::map<QString, QString> customData;
    //#== XML manipulation
    QDomElement toXML(QDomDocument& document) const override;
    void        readXML(QXmlStreamReader* xmlStream) override;
    XML_READER_DEFINE_FROM_XML(BaseMetadata);

    QString toDebugString(
        size_t indentationLevel = 0,
        bool   printEverything  = false, //
        int    recurseLevel     = -1) const;

    bool operator==(const BaseMetadata& other) const;

    static struct XMLTags {
        struct {
            const QString uuid        = "uuid";
            const QString description = "description";
            const QString metaNote    = "meta-note";

            /// \deprecated Superceded by creationDateTime
            const QString creationDate = "creation-date";
            /// \deprecated Superceded by creationDateTime
            const QString creationTime = "creation-time";
            /// \deprecated Superceded by editDateTime
            const QString editDate = "edit-date";
            /// \deprecated Superceded by editDateTime
            const QString editTime = "edit-time";

            const QString creationDateTime = "creation-date-time";
            const QString editDateTime     = "edit-date-time";
            const QString section          = "metadata-common";
            const QString tags             = "tags";
            const QString name             = "name";
            const QString customData       = "custom-data";
            const QString source           = "source";
            const QString exactName        = "exact-name";
            const QString version          = "version";
            const QString xmlVersion       = "xml-version";

            const size_t debugStringSize  = 20;
            const size_t nestedIndentSize = 2;
        } base;

        struct {
            const QString uuid           = "uuid";
            const QString exactName      = "exact-name";
            const QString descriptorName = "data-descriptor";
        } descriptor;

        struct {
            const QString path           = "path";
            const QString attachmentName = "attachment";
        } attachment;
    } xmlTags;
};
} // namespace qde
