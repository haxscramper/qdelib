#pragma once

#include "containeritem.hpp"

namespace qde {
/*!
 * \brief Classes derived from this one represent collection of data
 * (lists, tables, mind maps etc.)
 */
class DataContainer : public DataEntity
{
};
} // namespace qde
