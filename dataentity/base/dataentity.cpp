#include "dataentity.hpp"
#include <hdebugmacro/all.hpp>
#include <hsupport/misc.hpp>
#include <hsupport/xml.hpp>


namespace qde {

DataEntity::DataEntityXMLTags DataEntity::xmlTags;

/*!
 * \brief Constructor for DataEntity class. UUID, creationTime and
 * date will be set based on time of creation. To avoid this use
 * `createMetadata = false`.
 */
DataEntity::DataEntity(
    /// If true baseMetadata will be created
    bool createMetadata
    //
) {
    if (createMetadata) {
        baseMetadata.uuid = QUuid::createUuid();
        // baseMetadata.creationDate     = QDate::currentDate();
        // baseMetadata.creationTime     = QTime::currentTime();
        baseMetadata.creationDateTime = QDateTime::currentDateTime();
    }
}


//#  //////////////////////////////////////////////////////////////////////
//#  File-related
//#  //////////////////////////////////////////////////////////////////////


/// \brief Write conent of DataEntity to file located at path. If file
/// does not exist exception will be thrown
/// \todo Add which exception will be thrown
void DataEntity::writeFile(QString path, bool appendExtension) {
    if (appendExtension) {
        path += getExtension(true);
    }

    if (!QFile(path).exists()) {
        throw std::invalid_argument(
            "file does not exist " + path.toStdString());
    } else {
        QDomDocument document;
        document.appendChild(this->toXML(document));
        spt::writeDocument(document, path);
    }
}


/// \brief Read content of the file into fields of DataEntity. If it
/// is not possible exit function without throwing exception
void DataEntity::readFile(QString path, bool appendExtension) {
    if (appendExtension) {
        path += getExtension(true);
    }

    QFile file(path);
    if (!file.exists()) {

    } else if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        throw std::invalid_argument(
            "file cannot be opened " + path.toStdString());
    } else {
        QXmlStreamReader xmlStream;
        xmlStream.setDevice(&file);
        fromXML(&xmlStream);
    }
}


/*! \brief Write conent of DataEntity to file located at path. If it
 * is not possible exit function without throwing exception or create
 * new file.
 *
 * \return True if file was created and written succesfully
 */
bool DataEntity::saveFile(
    QString path,
    bool    appendExtension, ///< If true and path's end does not match
                             ///< default extension it will be apended
                             ///< with extension given by getExtension()
    bool autoCreatePath      ///< If true function will try to create file
                             ///< and all missing parent folders using
                             ///< QDir::mkpath
) {
    if (appendExtension && !path.endsWith(getExtension(true))) {
        path += getExtension(true);
    }

    if (autoCreatePath && !spt::createFilePath(path)) {
        return false;
    }

    bool success = false;
    try {
        writeFile(path);
        success = true;
    } catch (...) {}

    return success;
}


/// \brief Read content of the file into fields of DataEntity. If it
/// is not possible exit function without throwing exception
bool DataEntity::openFile(QString path, bool appendExtension) {
    if (appendExtension) {
        path += getExtension(true);
    }

    bool success;
    try {
        readFile(path);
        success = true;
    } catch (...) { success = false; }

    return success;
}

//#  //////////////////////////////////////////////////////////////////////
//#  Content-related
//#  //////////////////////////////////////////////////////////////////////


/// \brief Delete all data from DataEntity. Set creation date and
/// creation time into null state. \warning By default UUID is also deleted
void DataEntity::clearMetadata(bool delete_uuid) {
    if (delete_uuid) {
        baseMetadata.uuid = QUuid();
    }
    // baseMetadata.creationDate = QDate();
    // baseMetadata.creationTime = QTime();
    baseMetadata.creationDateTime = QDateTime();
}


/*!
 * \brief Clear all base metadate into null state. Create new uuid,
 * set creationDateTime as `currentDateTime()`
 */
void DataEntity::regenerateMetadata() {
    baseMetadata.uuid             = QUuid::createUuid();
    baseMetadata.creationDateTime = QDateTime::currentDateTime();
}


/// Set currentDateTime as editDateTime
void DataEntity::setCurrentTimeAsEdit() {
    baseMetadata.editDateTime = QDateTime::currentDateTime();
}


/*!
 * \brief Return default extension for each type of data
 * \return QString - extension with optionally prepended comma
 */
QString DataEntity::getExtension(bool prependDot ///< Prepend comma to
                                                 ///< extension
                                 ) const {
    BASE_CLASS_UNUSED(prependDot)
    DEBUG_BASE_CLASS_FUNCTION_WARNING
    return "";
}


/*!
 * \brief Recursively compare two DataEntities
 * \return True if all fields (creation date, creation time, uuid,
 * etc. are equal. Comparison stops as soon as inconsistency has been
 * found \note Because comparison is done recursively this operation
 * might become expensive for large container objects
 */
bool DataEntity::operator==(const DataEntity& other) const {
    return baseMetadata == other.baseMetadata;
}


/*!
 * \brief Recursively generate debug string going `recurseLevel` deep. If
 * recurse level is zero go only one level deep. If recurse level is
 * negative go deeper without any limitataions (default)
 */
QString DataEntity::toDebugString(
    [[maybe_unused]] size_t indentationLevel,
    bool                    printEverything,
    [[maybe_unused]] int    recurseLevel) {
    return baseMetadata.toDebugString(
        indentationLevel, printEverything, recurseLevel);
}
} // namespace qde
