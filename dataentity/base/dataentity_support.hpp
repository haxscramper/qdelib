#pragma once

//===    Qt    ===//
#include <QDomElement>
#include <QXmlStreamReader>


//=== Internal ===//
#include <hsupport/misc.hpp>
#include <hsupport/multi_select.hpp>
#include <hsupport/single_select.hpp>


//===    STL   ===//
#include <variant>


//=== Sibling  ===//
#include "xml_debug_macro.hpp"


namespace qde {
/*!
 * \brief Wrapper class that allows to manage simple types of data (such as
 * strings, nunmbers, datetime, url etc.) using single interface for
 * storing inside DataCotainer
 */
class SimpleData
{
  public:
    enum class Type
    {
        QString,
        QStringList,
        Int,
        Double,
        LongLong,
        QUrl,
        QDateTime,
        SingleSelect,
        MultiSelect
    };

    SimpleData() = default;
    SimpleData(QString _data);
    bool operator==(const SimpleData& other);

    template <class Content>
    void setContent(Content value) {
        content = std::move(value);
    }


    QDomElement toXML(QDomDocument& document);
    void        fromXML(QXmlStreamReader* xmlStream);
    Type        getContentType() const;

  private:
    using SimpleDataContent = std::variant<
        QString,
        QStringList,
        int,
        double,
        long long,
        QUrl,
        QDateTime,
        spt::SingleSelect,
        spt::MultiSelect>;


    SimpleDataContent content;
};
} // namespace qde
