#pragma once

/*!
 * \file dataentity.hpp
 */


//===    Qt    ===//
#include <QDateTime>
#include <QDomDocument>
#include <QDomElement>
#include <QDomNode>
#include <QFile>
#include <QStringList>
#include <QTextStream>
#include <QUuid>
#include <QXmlStreamReader>


//===   Boost  ===//
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>


//===    STL   ===//
#include <memory>
#include <string>


//=== Sibling  ===//
#include "base_metadata.hpp"
#include "dent_debug_macro.hpp"

//  ////////////////////////////////////


/// DataEntity-related classes for storing/parsing/writing data
namespace qde {
/// \brief Base class for information-management data objects
class DataEntity
{
  public:
    struct DataEntityXMLTags : BaseMetadata::XMLTags {
        struct {
            QString rootName = "dataentity-root";
        } dataentity;
    };


    explicit DataEntity(bool createMetadata = true);
    virtual ~DataEntity() = default;

    //# File-related
    virtual bool saveFile(
        QString path,
        bool    appendExtension = false,
        bool    autoCreatePath  = true);
    virtual bool openFile(QString path, bool appendExtension = false);
    virtual void writeFile(QString path, bool appendExtension = false);
    virtual void readFile(QString path, bool appendExtension = false);


    //# Content-related
    virtual QDomElement toXML(QDomDocument& document) const = 0;
    // TODO make static
    virtual void    fromXML(QXmlStreamReader* xmlStream) = 0;
    virtual void    clearMetadata(bool delete_uuid = true);
    virtual void    regenerateMetadata();
    void            setCurrentTimeAsEdit();
    virtual QString getExtension(bool prependDot = false) const = 0;
    bool            operator==(const DataEntity& other) const;

    inline void printToDebug() {
        std::cout << toDebugString().toStdString();
    }

    virtual QString toDebugString(
        size_t indentationLevel = 0,
        bool   printEverything  = false, //
        int    recurseLevel     = -1)
        = 0;

    //# Metadata-related function access
    QString     getMetaDescription() const;
    void        setMetaDescription(QString description);
    void        addMetaTag(spt::Tag tags);
    void        addMetaTags(const spt::TagSet& tags);
    void        setMetaTags(const spt::TagSet& tags);
    spt::TagSet getMetaTags();
    void        setCustomData(std::pair<QString, QString> data);
    void        setMetaSource(QString source);
    void        setCreationNow();
    QString     getMetaNote() const;
    void        setMetaNote(QString notes);
    QString     getMetaName() const;
    void        setMetaName(QString name);
    void        setExactName(const QString& name);
    QUuid       getUUID() const;


    DataDescriptor      asDescriptor() const;
    BaseMetadata        getBaseMetadata() const;
    void                setBaseMetadata(const BaseMetadata& value);
    BaseMetadata&       getBaseMetadataRef();
    const BaseMetadata& getBaseMetadataRef() const;

  protected:
    BaseMetadata             baseMetadata;
    static DataEntityXMLTags xmlTags;
};
} // namespace qde

Q_DECLARE_METATYPE(qde::DataEntity*)
