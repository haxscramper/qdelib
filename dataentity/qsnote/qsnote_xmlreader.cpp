


#include "qsnote_xmlreader.hpp"
#include "qsnote.hpp"
#include <hdebugmacro/all.hpp>


namespace qde {
namespace xml {
    static void readQSNoteContent(
        QSNote*                target,
        QXmlStreamReader*      xmlStream,
        QSNote::QSNoteXMLTags* tags) {
        while (xmlStream->readNextStartElement()) {
            QString elementName = xmlStream->name().toString();

            if (elementName == tags->qsnote.text) {
                target->setNoteText(xmlStream->readElementText());
            } else if (elementName == tags->qsnote.type) {
                target->setNoteType(xmlStream->readElementText());
            } else if (elementName == tags->qsnote.tags) {
                target->setNoteTags(
                    spt::TagSet::fromXML(xmlStream, tags->qsnote.tags));
            } else {
                xmlStream->skipCurrentElement();
            }
        }
    }


    void readQSNoteXML(
        QSNote*           target,
        QXmlStreamReader* xmlStream,
        void*             _tags) {
        if (xmlStream->name().isEmpty()) {
            xmlStream->readNextStartElement();
        }

        QSNote::QSNoteXMLTags* tags;
        if (_tags == nullptr) {
            tags = &target->xmlTags;
        } else {
            tags = static_cast<QSNote::QSNoteXMLTags*>(_tags);
        }


        while (xmlStream->readNextStartElement()) {
            QString elementName = xmlStream->name().toString();

            if (elementName == tags->base.section) {
                target->getBaseMetadataRef().readXML(xmlStream);
            } else if (elementName == tags->qsnote.content) {
                readQSNoteContent(target, xmlStream, tags);
            } else {
                xmlStream->skipCurrentElement();
            }
        }
    }
} // namespace xml
} // namespace qde
