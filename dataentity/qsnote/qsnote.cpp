#include "qsnote.hpp"
#include "qsnote_converter.hpp"
#include <hsupport/misc.hpp> // TODO Is this needed?


namespace qde {

QSNote::QSNoteXMLTags QSNote::xmlTags;

/// \brief Convert note to target type.
/// Changes note type to targetType and sets note text to converted
void QSNote::convertTo(QSNote::NoteType targetType) {
    QSNote_Converter converter;
    converter.convert(this, targetType);
    setNoteType(targetType);
}
} // namespace qde
