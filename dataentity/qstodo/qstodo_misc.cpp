#include "qstodo.hpp"
#include <halgorithm/all.hpp>


namespace qde {
spt::TagSet QSTodo::getTodoTags() const {
    return todoTags;
}


QSTodo::CompletionLikelihood QSTodo::getCompletionLikelihood() const {
    return completionLikelihood;
}

void QSTodo::setCompletionLikelihood(
    const QSTodo::CompletionLikelihood& value) {
    completionLikelihood = value;
}


void QSTodo::setTodoTags(const spt::TagSet& value) {
    todoTags = value;
}


QDateTime QSTodo::getDeadline() const {
    return deadline;
}


void QSTodo::setDeadline(const QDateTime& value) {
    deadline = value;
}

void QSTodo::setEstimatedTime(QSTodo::EstimatedTime _estimatedTime) {
    estimatedTime = _estimatedTime;
}

void QSTodo::setPriority(QSTodo::Priority _priority) {
    priority = _priority;
}

void QSTodo::setStatus(QSTodo::Status _status) {
    status = _status;
}

void QSTodo::setNecessity(QSTodo::Necessity _necessity) {
    necessity = _necessity;
}

void QSTodo::addTodoTag(spt::Tag newTag) {
    todoTags.addTag(newTag);
}


void QSTodo::addTodoTags(spt::TagSet newTags) {
    todoTags.addTags(newTags);
}

void QSTodo::setTaskDescription(QString description) {
    taskDescription = description;
}

QString QSTodo::getTaskDescription() const {
    return taskDescription;
}

void QSTodo::appendNestedTodo(QSTodoUptr nestedTodo) {
    nested.push_back(std::move(nestedTodo));
}

void QSTodo::setTimeRanges(spt::DateTimeRanges ranges) {
    timeRanges = ranges;
}

void QSTodo::setAttachments(const std::vector<Attachment>& _attachments) {
    attachments = _attachments;
}

void QSTodo::addAttachment(const Attachment& _attachment) {
    attachments.push_back(_attachment);
}

void QSTodo::setComments(const std::vector<QString>& _comments) {
    comments = _comments;
}

void QSTodo::setWeight(int _weight) {
    weight = _weight;
}

bool QSTodo::dependsOn(const QSTodo& todo) const {
    return std::find(
               dependencies.begin(),
               dependencies.end(),
               todo.asDescriptor())
           != dependencies.end();
}

void QSTodo::addDependency(const QSTodo& todo) {
    dependencies.push_back(todo.asDescriptor());
}

void QSTodo::addDependency(const DataDescriptor& todo) {
    dependencies.push_back(todo);
}


void QSTodo::removeDependecy(const QSTodo& todo) {
    spt::erase_all(dependencies, todo.asDescriptor());
}


/// \brief Insert new todo in list
bool QSTodo::insertNestedTodo(
    QSTodoUptr nestedTodo, ///< New QSTodo to insert
    QSTodo*    insert,     ///< Insert before/after this one
    bool insertAfter ///< true: new one will be inserted after. false:
                     ///< new one will be inserted before
) {
    auto iter = std::find_if(
        nested.begin(), nested.end(), [insert](const QSTodoUptr& todo) {
            return todo.get() == insert;
        });

    if (iter != nested.end()) {
        if (insertAfter) {
            ++iter;
            if (iter != nested.end()) {
                nested.insert(iter, std::move(nestedTodo));
            } else {
                nested.push_back(std::move(nestedTodo));
            }
        } else {
            nested.insert(iter, std::move(nestedTodo));
        }
        return true;
    } else {
        return false;
    }
}

std::vector<QSTodoUptr>& QSTodo::getNestedTodoRef() {
    return nested;
}

} // namespace qde
