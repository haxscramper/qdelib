#include "qsimage_xmlreader.hpp"


namespace qde {
namespace xml {
    QSImage_XMLReader::QSImage_XMLReader(QXmlStreamReader* _xmlStream)
        : DataEntity_XMLReader(_xmlStream) {
    }

    void QSImage_XMLReader::setTarget(QSImage* _target) {
        target = _target;
        tags   = &target->xmlTags;
    }

    void QSImage_XMLReader::readXML() {
    }
} // namespace xml
} // namespace qde
