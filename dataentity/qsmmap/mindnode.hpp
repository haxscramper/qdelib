#pragma once

#include <node.hpp>
#include "mindedge.hpp"
#include <QColor>
#include <QRectF>

class MindNode : public Node
{
  public:
    MindNode();

    struct Appearance {
        Appearance() { edgeRound = 1; }
        QColor color;
        QRectF outline;
        float  edgeRound;
    };

    enum class Shape { Circle, Diamond, Rectangle };

    QList<Port*> getPorts() const;
    void         setPorts(const QList<Port*>& value);

    QVariant getData() const;
    void     setData(const QVariant& value);

  private:
    QVariant     data;
    QList<Port*> ports;
};

