#pragma once

#include <QGraphicsScene>
#include <memory>
#include <QDebug>

#include <datacontainer.hpp>


namespace dent {
class MindMapView;

class QSMmap : public DataContainer
{
  public:
    QSMmap();
    ~QSMmap();

    // DataObject interface
  public:
    QString getName() override { return "qsmmap"; }
};
} // namespace dent
Q_DECLARE_METATYPE(dent::QSMmap*)

