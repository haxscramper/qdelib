#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
cd "$(dirname "$0")"
set -o nounset
set -o errexit

{ echo ../build/libqdelib.a ; ls *.hpp *.cpp; } | entr "test.sh"

