#include "support.hpp"
#include <qdelib/qslink.hpp>


TEST(QSLink, Write) {
    qde::QSLink link;
    QString     path = getTestDir() + "/qslink/write_test";
    ASSERT_TRUE(link.saveFile(path, true));
}


TEST(QSLink, Read) {
    qde::QSLink link;
    QString     path = getTestDir() + "/qslink/write_test";
    ASSERT_TRUE(link.openFile(path, true));
}


TEST(QSLink, WriteReadEqual) {
    qde::QSLink link_1;
    qde::QSLink link_2;

    puts("D");

    link_1.setURL(spt::getRandomBase32(100));
    link_1.setContentText(spt::getRandomBase32(100));
    link_1.setLinkTags(spt::getRandomBase64List());
    setRandomValues(link_1);

    puts("D");

    QString path = getTestDir() + "/qslink/read_write_test";

    puts("---");

    ASSERT_TRUE(link_1.saveFile(path, true));

    puts("ds");

    ASSERT_TRUE(link_2.openFile(path, true));
    ASSERT_TRUE(link_1 == link_2);
}
