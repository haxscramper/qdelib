#!/usr/bin/env python

import requests
from fabulous.color import bold, green, red
import json
from common import *

keys = json.load(open("pocket-tokens.json"))

print("Sending request for all items")

pocket_add = requests.post(
    'https://getpocket.com/v3/get',
    data={
        'consumer_key': keys["consumer_key"],
        'access_token': keys["access_token"],
        'state': "all"
    })

printReturn(pocket_add.status_code)
print("Done")

with open("tmp.pocket_out/raw/url_get.json", "w") as outfile:
    json.dump(pocket_add.json(), outfile)

print("Returned json was saved at", bold("tmp.pocket_out/raw/url_get.json"))
