#! /usr/bin/env python
import sys
import os
import json
import codecs
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from QDELib import QSNote


def get_new_note_name(note_folder: str) -> str:
    """
    Generate new name for the note that is not present in the folder.

    Names are are generated as untitled{}.qslink where {} is replaced
    with number and tested until new name is found that does not exist
    """
    idx: int = 0

    res = "untitled{}.qsnote".format(idx)
    out = "{}/{}".format(note_folder, res)
    while os.path.exists(out):
        res = "untitled{}.qsnote".format(idx)
        out = "{}/{}".format(note_folder, res)
        idx += 1

    return res


class App(QWidget):
    def __init__(self):
        super().__init__()
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.title = "text2note"
        self.text_edit = QTextEdit()
        self.relative_save_path = QLabel()
        self.save_dir = QLineEdit()
        self.final_path = QLabel()
        self.save_dir_preview = QListView()
        self.save_dir_model = QFileSystemModel()

        self.initUI()
        self.updateUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        save_text = QPushButton("Save new")
        save_text.clicked.connect(self.save_new_note)
        self.save_dir.returnPressed.connect(self.updateUI)

        self.save_dir_model.setFilter(QDir.NoDotAndDotDot | QDir.Files)
        self.save_dir_preview.setModel(self.save_dir_model)

        layout = QGridLayout()
        self.setLayout(layout)

        layout.addWidget(self.text_edit, 1, 1, 1, 2)

        layout.addWidget(save_text, 2, 1)
        layout.addWidget(self.relative_save_path, 2, 2)
        layout.setColumnStretch(2, 3)

        save_dir_label = QLabel()
        save_dir_label.setText("Save dir")
        layout.addWidget(self.save_dir, 3, 2)
        layout.addWidget(save_dir_label, 3, 1)
        layout.addWidget(self.final_path, 4, 1, 1, 2)

        layout.addWidget(self.save_dir_preview, 1, 3, 1, 4)

        self.relative_save_path.setText(self.get_new_note_name())
        self.save_dir.setText(os.getcwd())

        self.show()

    def get_save_folder(self) -> str:
        return self.save_dir.text()

    def get_new_note_name(self) -> str:
        return get_new_note_name(self.get_save_folder())

    def updateUI(self) -> None:
        self.save_dir_model.setRootPath(self.get_save_folder())
        self.save_dir_preview.setRootIndex(
            self.save_dir_model.index(self.get_save_folder()))
        self.final_path.setText(
            os.path.join(self.get_save_folder(), self.get_new_note_name()))
        self.relative_save_path.setText(self.get_new_note_name())

    @Slot()
    def save_new_note(self) -> None:
        abs_path: str = os.path.join(self.get_save_folder(),
                                     self.get_new_note_name())
        note = QSNote()
        # TODO Set note type
        note.setNoteText(self.text_edit.toPlainText())
        note.saveFile(abs_path)

        self.updateUI()
        self.text_edit.clear()

    def set_save_folder(self, target_dir: str) -> None:
        self.save_dir.setText(target_dir)
        self.updateUI()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    ex.set_save_folder(os.path.join(os.getcwd(), "tmp.text2note_out"))
    sys.exit(app.exec_())
