#!/usr/bin/env python
# Download all tasks from the user

from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from typing import List, MutableMapping
from QDELib import QSTodo
import json
import os

# If modifying these scopes, delete the file token.json.
SCOPES = 'https://www.googleapis.com/auth/tasks.readonly'


class Task:
    def __init__(self):
        self.id = None
        self.has_parent = False
        self.google_task = None
        self.task_list = None
        self.parent_id = None
        self.child_tasks: MutableMapping['str', Task] = {}

    def get_description(self) -> str:
        res: str = self.google_task['title'] + "\n\n"

        if 'notes' in self.google_task:
            res = res + self.google_task['notes']

        return res


def fetch_tasks() -> MutableMapping[str, Task]:
    """Shows basic usage of the Tasks API.
    Prints the title and ID of the first 10 task lists.
    """
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    store = file.Storage('google-downloader-token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('google-credentials.json',
                                              SCOPES)
        creds = tools.run_flow(flow, store)
    service = build('tasks', 'v1', http=creds.authorize(Http()))

    # Call the Tasks API
    results = service.tasklists().list().execute()
    lists = results.get('items', [])

    task_dict: MutableMapping[str, Task] = {}

    for list in lists:
        tasks = service.tasks().list(tasklist=list['id']).execute().get(
            'items', [])
        for task in tasks:
            task_wrap = Task()
            task_wrap.google_task = task
            task_wrap.task_list = list['title']
            if 'parent' in task:
                task_wrap.has_parent = True
                task_wrap.parent_id = task['id']

            task_wrap.id = task['id']
            print(" -> Retrieved", task['id'][20:])

            task_dict[task_wrap.id] = task_wrap

    return task_dict


if __name__ == '__main__':
    task_list: List[Task] = fetch_tasks()

    for id, task in task_list.items():
        qstodo = QSTodo()
        qstodo.setTaskDescription(task.get_description())
        qstodo.setCustomData("google-task-downloader",
                             json.dumps(task.google_task))

        path = os.path.join(os.getcwd(), "saved-gtask", task.task_list,
                            task.google_task['id'] + ".qstodo")

        qstodo.saveFile(path)
