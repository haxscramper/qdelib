#include <boost/spirit/home/x3.hpp>
#include <iostream>
#include <string>

using namespace boost::spirit;

int main() {
    std::string str
        = "Random notes string that contains several\n"
          "line breaks, #tags, #more_tags, @metatags: content goes here;"
          "Start of the list\n"
          "* First _item_\n"
          "** Nested *item* that is also bold";

    auto start = str.begin();
    auto end   = str.end();

    bool r = x3::phrase_parse(
        start,
        end,
        // ---- parser start ----
        *(x3::char_('#') >> (*(x3::char_("a-zA-Z_0-9")))[([](auto& ctx) {
              std::cout << "Found tag\n";
          })]
          | x3::char_('@') >> (*(x3::char_("a-zA-Z_0-9")))[([](auto& ctx) {
                std::cout << "Found meta tag\n";
            })] >> x3::char_(':', ';')[([](auto& ctx) {
                std::cout << "Found meta tag content\n";
            })] //
          | x3::omit[x3::char_]),
        // ----  parser end  ----
        x3::ascii::space);


    return 0;
}
