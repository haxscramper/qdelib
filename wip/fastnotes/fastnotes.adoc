This markup language is desined to be used in scratch notes.
Imagine the situation: you have to quickly write something down,
and don't really care about how good it will look afterwards.
Your only concert is to make sure that everyhting you currently
have in your head is written down and nothing is lost. Designing
language for task like this imposes several constraints but also
gives number of interesting possibilities.

Constraints::
  ** You have to optimize this language as write-once-read-once. It's
     main use case: you write something, _maybe_ read it later and
     then convert into some other markup language of choice
  ** Take into account mobile device users. All language constructs
     have to be easy to type and not dependent on indentation/font
     etc.
Advantages::
  ** No need to support table of contents, chapters, tables etc.
  ** No actual need to write html/latex generator


== Syntax description

Tags::
  Tag is denoted as `#sometag` where `sometag` is a word without
  whitespaces. First whitespace denotes end of the tag.
Insert-later::
  When you write something and don't know how it is exactly called
  or you will name it later but right now you want to just say
  I mean this thing here, but I will insert it later. To denote
  something like this use `$(you thing you want to insert)`.
Author note::
  Additional commentaries for readers: `@[Your text goes here]`
Choose-one::
  You don't know what to write at this place actually, and just
  write down all possible things that you _might_ place here. To
  do this use `(? cat or dog?)` syntax.
Bold/Italic/Monospace::
  Identical to the asciidoctor.
  Bold:::
    Bold text is denoted as `\*bold\*`
  Italic:::
    Italic text is denoted as `\_italic\_`
  Monospace::
    Monospace text is denoted as `\`monospace\`` or as `|monspace|`.
    Second version is made for typing on mobile keyboards.
Lists::
  Identical to asciidoctor in most of the cases for line-based lists.
  Inline lists (i.e. multiple items on one long line) can be created
  using `@::(1) first item; (2) second item; (3) last item;;` for
  numbered lists. Automatically numbered lists can be created using 
  `(.)` and bullet lists can be created using `(+)`.
Metatags::
  In some cases you might need a tag that has additional meaning
  for each of the occurencies. For example: you are writing something
  and want to make some kind of remark. Adding another combination of
  charcters to denote special environment for remarks, notes etc. is
  not really a good idea. But you can use `@remark:your remark goes here;`
  Everything between `:` and `;` is treated as metatag argument. Metatags
  supported by default are `@note, @remark, @idea, @warning`.

